<p class="has-line-data" data-line-start="0" data-line-end="3">This is a server build for LYRA sample project.<br>
Here is the UE5 docs on setting up dedicated game servers.<br>
<a href="https://docs.unrealengine.com/5.3/en-US/setting-up-dedicated-servers-in-unreal-engine/">https://docs.unrealengine.com/5.3/en-US/setting-up-dedicated-servers-in-unreal-engine/</a></p>
<p class="has-line-data" data-line-start="4" data-line-end="7">I am using Microk8 on ubuntu.<br>
The following documentation explains how to use MicroK8s with local images, or images fetched from public or private registries.<br>
<a href="https://microk8s.io/docs/registry-images">https://microk8s.io/docs/registry-images</a></p>
<p class="has-line-data" data-line-start="8" data-line-end="10">This repo includes a Dockerfile and some Kubernetes files to create a gamerserver and or a fleet of gamerservers using agones.<br>
Agones Docs: <a href="https://agones.dev/site/">https://agones.dev/site/</a></p>
<p class="has-line-data" data-line-start="11" data-line-end="14">To build an image<br>
docker build -t localhost:32000/lyra-server:v1.0.1 -f Dockerfile .<br>
docker push localhost:32000/lyra-server:v1.0.1</p>
<p class="has-line-data" data-line-start="15" data-line-end="18">kubectl create -f ./gameserver.yaml<br>
kubectl create -f ./fleet.yaml<br>
kubectl scale fleet simple-game-server --replicas=5</p>
<p class="has-line-data" data-line-start="19" data-line-end="22">To connect your client to a game server run the client using:<br>
.\LyraClient.exe 192.168.1.7:7777 -WINDOWED -ResX=800 -ResY=450 -log<br>
Use the server ip and the port on the fleet or gameserver.</p>