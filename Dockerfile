##
## a basic container for an unreal game server
##

# Base our Docker image off Ubuntu 20.04.
FROM ubuntu:20.04
RUN mkdir /app
# Add the game server binaries to the image.
COPY ./LinuxServer /app
# Create a user to run our game server binaries as. If we didn't do this, our binaries
# would run as ue5
RUN useradd -m ue5
RUN chown -R ue5:ue5 /app
USER ue5


# expose ports for the container
EXPOSE 7777/tcp
EXPOSE 7777/udp

# Tell Docker that it should use the launch script when the container is executed.
ENTRYPOINT [ "/app/LyraServer.sh" ]
